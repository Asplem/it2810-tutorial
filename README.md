This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## Setting up before starting tutorial: ##

1. git clone https://bitbucket.org/Asplem/it2810-tutorial/
2. cd it2810-tutorial
3. npm install
4. npm start

The app will then run at localhost:3000.