import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import JasmineEnzyme from 'jasmine-enzyme';
import App from '../app/App';
import AppComponent from '../app/App';

describe("App", function () {
    let wrapper = null;

    beforeEach(function () {
        JasmineEnzyme();
        wrapper = shallow(<App />);
    });

    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<App />, div);
    });

    it('renders <AppComponent />', () => {
        expect(wrapper.find(AppComponent)).toBeDefined();
    });

});
