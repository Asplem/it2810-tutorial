import React from 'react';

import { shallow } from 'enzyme';
import JasmineEnzyme from 'jasmine-enzyme';

import AppComponent from '../app/AppComponent.js';


describe("AppComponent", function () {
    let wrapper = null;

    beforeEach(() => {
        wrapper = shallow(<AppComponent/>);
        JasmineEnzyme();
    });

    it("should have two \"counter-button\"s", function () {
        expect(wrapper).toBeDefined();
        expect(wrapper.find('.counter-button')).toBeDefined();
    });

    it("renders with state.count = 0", function() {
        expect(wrapper).toHaveState('counter', 0);
    });
});
