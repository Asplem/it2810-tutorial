import React, { Component } from 'react';
import './App.css';

export default class AppComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 0
        };
    }
    render() {
        return (
            <div>
                <h1>Test this component</h1>
                <div className="counter-div">
                    <button className="counter-button" onClick={this.decrement.bind(this)}>Decrease</button>
                    <button className="counter-button" onClick={this.increment.bind(this)}>Increase</button>
                    <p>Count: {this.state.counter}</p>
                </div>
            </div>
        );
    }
    increment(){
        var count = this.state.counter + 1;
        this.setState({
            counter: count
        });
    }
    decrement(){
        var count = this.state.counter - 1;
        this.setState({
            counter: count
        });
    }
}
